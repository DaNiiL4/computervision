#include "imageutils.h"
//#include "math.h"

namespace iutils {
    const double gradXData[] = {-1, 0, 1,
                            -2, 0, 2,
                            -1, 0, 1};
    const Matrix gradXkernel(3, 3, gradXData);

    const double gradYData[] = {-1, -2, -1,
                             0,  0,  0,
                             1,  2,  1};
    const Matrix gradYkernel(3, 3, gradYData);

    std::shared_ptr<Matrix> loadImage(const QString &fileName) {
        QImage qImage;
        bool result = qImage.load(fileName);
        if(result){
            const int rows = qImage.height();
            const int columns = qImage.width();
            auto matrix = std::make_shared<Matrix>(rows, columns);

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    QRgb color = qImage.pixel(j, i);
                    int r = qRed(color),
                        g = qGreen(color),
                        b = qBlue(color);

                    double value = (0.2126 * r + 0.7152 * g + 0.0722 * b) / 255.0;
                    matrix->set(i, j, value);
                }
            }
            matrix->normalize();
            return matrix;
        }
        else
            return nullptr;
    }

    bool saveImage(const Matrix &matrix, const QString &fileName) {
        const int width = matrix.getColumnsCount();
        const int height = matrix.getRowsCount();
        auto qImage = std::make_shared<QImage>(width, height, QImage::Format_RGB32);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                double value = matrix.get(i, j);
                int color = std::min(255, (int) (value * 255));
                qImage->setPixel(j, i, qRgb(color, color, color));
            }
        }
        return qImage->save(fileName);
    }

    void calcBorderLine(const Matrix &matrix, const Matrix &kernel, Matrix &result, int rowFrom, int rowTo, int columnFrom, int columnTo, BorderType border) {
        int kRows = kernel.getRowsCount() / 2;
        int kColumns = kernel.getColumnsCount() / 2;

        for(int i = rowFrom; i < rowTo; i++) {
            for(int j = columnFrom; j < columnTo; j++) {
                double val = 0;
                for(int u = -kRows; u <= kRows; u++) {
                    for(int v = -kColumns; v <= kColumns; v++) {
                        val += matrix.getBorderElement(i - u, j - v, border) * kernel.get(u + kRows, v + kColumns);
                        if(i==0 && j==0) {
                            std::cout << val << "|" << matrix.getBorderElement(i - u, j - v, border) << std::endl;
                        }
                    }
                }
                result.set(i, j, val);
            }
        }
    }

    void convolution(const Matrix &matrix, const Matrix &kernel, Matrix &result, BorderType border) {
        int matrixRows = matrix.getRowsCount();
        int matrixColumns = matrix.getColumnsCount();
        int kernelRows = kernel.getRowsCount();
        int kernelColumns = kernel.getColumnsCount();
        int kRows = kernelRows / 2;
        int kColumns = kernelColumns / 2;

        result.resize(matrixRows, matrixColumns);
        const double *kernelData = kernel.getData();
        const double *data = matrix.getData();
        double *resultData = result.getData();

        int innersquareMinRow = kRows;
        int innersquareMaxRow = matrixRows - kRows;
        int innersquareMinColumn = kColumns;
        int innersquareMaxColumn = matrixColumns - kColumns;

        //inner square
        for(int i = innersquareMinRow; i < innersquareMaxRow; i++) {
            for(int j = innersquareMinColumn; j < innersquareMaxColumn; j++) {
                double val = 0;
                for(int u = -kRows; u <= kRows; u++) {
                    for(int v = -kColumns; v <= kColumns; v++) {
                        val += data[(i - u) * matrixColumns + (j - v)] * kernelData[(u + kRows) * kernelColumns + (v + kColumns)];
                    }
                }
                resultData[i * matrixColumns + j] = val;
            }
        }

        //top
        calcBorderLine(matrix, kernel, result, 0, innersquareMinRow, 0, innersquareMaxColumn, border);

        //right
        calcBorderLine(matrix, kernel, result, 0, innersquareMaxRow, innersquareMaxColumn, matrixColumns, border);

        //bottom
        calcBorderLine(matrix, kernel, result, innersquareMaxRow, matrixRows, innersquareMinColumn, matrixColumns, border);

        //left
        calcBorderLine(matrix, kernel, result, innersquareMinRow, matrixRows, 0, innersquareMinColumn, border);
    }

    void calcGradient(const Matrix &x, const Matrix &y, Matrix &result) {
        result.resize(x.getRowsCount(), x.getColumnsCount());
        double *resultData = result.getData();
        const double *xData = x.getData();
        const double *yData = y.getData();
        for(int i = 0; i < result.getDataSize(); i++) {
            resultData[i] = sqrt(xData[i] * xData[i] + yData[i] * yData[i]);
        }
    }

    void sobel(const Matrix &matrix, Matrix &gradX, Matrix &gradY, Matrix &grad, BorderType border) {
        convolution(matrix, gradXkernel, gradX, border);
        convolution(matrix, gradYkernel, gradY, border);
        calcGradient(gradX, gradY, grad);
    }

    void gaus(const Matrix &matrix, Matrix &result, double sigma, BorderType border) {
        int size = std::max(qRound(sigma) * 6 - 1, 5);

        Matrix kernel(size, size);

        double twoSigmaSquare = 2 * sigma * sigma;
        double div = twoSigmaSquare * M_PI;

        for(int i = 0, x = - size / 2; i < size; i++, x++) {
            for(int j = 0, y = - size / 2; j < size; j++, y++) {
                kernel.set(i, j, (exp(- (x * x + y * y) / twoSigmaSquare)) / div);
            }
        }

        convolution(matrix, kernel, result, border);
    }

    void gausSeparate(const Matrix &matrix, Matrix &result, double sigma, BorderType border) {
        int size = qRound(sigma) * 6 + 1;

        double data[size];

        double twoSigmaSquare = 2 * sigma * sigma;
        double div = sqrt(2* M_PI) * sigma;

        for(int i = 0, x = - size / 2; i < size; i++, x++) {
            data[i] = (exp(- (x * x) / twoSigmaSquare)) / div;
        }

        Matrix tmp;
        Matrix rowKernel(1, size, data);
        Matrix columnKernel(size, 1, data);

        convolution(matrix, rowKernel, tmp, border);
        convolution(tmp, columnKernel, result, border);
    }

    void downSample(const Matrix &matrix, Matrix &result)
    {
        const int rowSize = matrix.getRowsCount();
        const int columnSize = matrix.getColumnsCount();
        result.resize((rowSize + 1) / 2, (columnSize + 1) / 2);
        for (int di = 0, si = 0; si < rowSize; di++, si += 2)
            for (int dj = 0, sj = 0; sj < columnSize; dj++, sj += 2)
                result.set(di, dj, matrix.get(si, sj));
    }
}
