QT += core gui
#QT -= gui

CONFIG += c++14

TARGET = ComputerVision
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    matrix.cpp \
    imageutils.cpp \
    pyramid.cpp

HEADERS += \
    matrix.h \
    common.h \
    imageutils.h \
    pyramid.h

DISTFILES +=
