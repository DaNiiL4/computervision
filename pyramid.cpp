#include "pyramid.h"

Pyramid::Pyramid()
{

}

Pyramid::Pyramid(const Matrix &matrix, const int octavesNumber, const int scalesPerOctave, const double sigma, const double baseSigma) :
                  octavesNumber(octavesNumber), scalesPerOctave(scalesPerOctave), size(octavesNumber * scalesPerOctave), firstSigma(sigma) {
    sigmas = std::make_unique<double[]>(size);
    scales = std::make_unique<Matrix[]>(size);

    double k = pow(2.0, 1.0 / scalesPerOctave);
    sigmas[0] = sigma;

    double sigmaDelta = sqrt(sigmas[0] * sigmas[0] - baseSigma * baseSigma);
    iutils::gausSeparate(matrix, scales[0], sigmaDelta);

    double nextLevelSigma, localSigma = sigma;
    for(int i = 1; i < size; i++) {
        sigmas[i] = sigmas[i - 1] * k;

        if(i % scalesPerOctave == 0) {
            iutils::downSample(scales[i - 1], scales[i]);
            localSigma = sigma;
        } else {
            nextLevelSigma = localSigma * k;
            sigmaDelta = sqrt(nextLevelSigma * nextLevelSigma - localSigma * localSigma);
            iutils::gausSeparate(scales[i - 1], scales[i], sigmaDelta);
            localSigma = nextLevelSigma;
        }
    }
}


Matrix &Pyramid::getScale(int octave, int scale) {
    return scales[octave * scalesPerOctave + scale];
}

double Pyramid::getSigma(int octave, int scale) {
    return sigmas[octave * scalesPerOctave + scale];
}

double Pyramid::get(int i, int j, double sigma) const
{
    sigma = std::min(std::max(sigma, firstSigma), sigmas[size - 1]);
    auto begin = &sigmas[0];
    auto upper = std::upper_bound(begin, begin + size, sigma);
    int index = upper - begin - 1;

    int numberOfDownscales = int(index / scalesPerOctave) + 1;

    return scales[index].get(i / numberOfDownscales, j / numberOfDownscales);
}
