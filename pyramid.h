#pragma once
#include "imageutils.h"

class Pyramid
{
private:
    double firstSigma;
    int octavesNumber;
    int scalesPerOctave;
    int size;
    std::unique_ptr<double[]> sigmas;
    std::unique_ptr<Matrix[]> scales;
public:
    Pyramid();
    Pyramid(const Matrix &matrix, const int octavesNumber, const int scalesPerOctave, const double sigma = 1, const double firstSigma = 0.5);
    Matrix &getScale(int octave, int scale);
    double getSigma(int octave, int scale);
    double get(int i, int j, double sigma) const;
};
