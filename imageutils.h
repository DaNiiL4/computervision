#include <QImage>
#include "matrix.h"

namespace iutils {
    std::shared_ptr<Matrix> loadImage(const QString &fileName);
    bool saveImage(const Matrix &matrix, const QString &fileName);
    void calcBorderLine(const Matrix &matrix, const Matrix &kernel, Matrix &result, int rowFrom, int rowTo, int columnFrom, int columnTo, BorderType border);
    void convolution(const Matrix &matrix, const Matrix &kernel, Matrix &result, BorderType border = BorderType::ZERO);
    void calcGradient(const Matrix &x, const Matrix &y, Matrix &result);
    void sobel(const Matrix &matrix, Matrix &gradX, Matrix &gragY, Matrix &grad, BorderType border = BorderType::ZERO);
    void gaus(const Matrix &matrix, Matrix &result, double sigma, BorderType border = BorderType::ZERO);
    void gausSeparate(const Matrix &matrix, Matrix &result, double sigma, BorderType border = BorderType::ZERO);
    void downSample(const Matrix &matrix, Matrix &result);
}
