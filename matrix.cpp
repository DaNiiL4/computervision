#include "matrix.h"

Matrix::Matrix()
{

}

Matrix::Matrix(int rows, int columns) : rows(rows), columns(columns), size(rows * columns) {
    data = std::make_unique<double[]>(size);
}

Matrix::Matrix(int rows, int columns, const double *data) : rows(rows), columns(columns), size(rows * columns) {
    this->data = std::make_unique<double[]>(size);
    for (int i = 0; i < size; ++i) {
        this->data[i] = data[i];
    }
}

double Matrix::getBorderElement(int i, int j, BorderType border) const{
    if(hasElement(i, j)) {
        return data[i * columns + j];
    }

    switch (border) {
    case BorderType::ZERO:
        return 0;
    case BorderType::COPY:
        i = std::max(0, std::min(rows - 1, i));
        j = std::max(0, std::min(columns - 1, j));
        return data[i * columns + j];
    default:
        return 0;
    }
}

void Matrix::resize(int rows, int columns) {
    this->rows = rows;
    this->columns = columns;
    size = rows * columns;
    data = std::make_unique<double[]>(size);
}

void Matrix::normalize() {
    auto minMax = std::minmax_element(data.get(), data.get() + size);
    double minVal = *minMax.first;
    double maxVal = *minMax.second;

    for(int i = 0; i < size; i++) {
        data[i] = (data[i] - minVal) / (maxVal - minVal);
    }
}


