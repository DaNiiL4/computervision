#include <iostream>
#include "pyramid.h"

void lab1() {
    auto matrix = iutils::loadImage("lena.png");
    bool saveResult = iutils::saveImage(*matrix, "bw.png");
    std::cout << saveResult;

    Matrix gradX, gradY, grad;
    iutils::sobel(*matrix, gradX, gradY, grad);
    gradX.normalize();
    gradY.normalize();
    grad.normalize();

    iutils::saveImage(gradX, "gradX.png");
    iutils::saveImage(gradY, "gradY.png");
    iutils::saveImage(grad, "grad.png");
}

void lab2() {
    auto matrix = iutils::loadImage("lena.png");
    bool saveResult = iutils::saveImage(*matrix, "bw.png");
    std::cout << saveResult;

    Pyramid pyramid(*matrix, 5, 5);

    for(int i = 0; i < 5; i++) {
        for(int j = 0; j < 5; j++) {
            iutils::saveImage(pyramid.getScale(i, j), QString("pyramid") + QString::number(pyramid.getSigma(i, j)) + ".png");
        }
    }

}

int main(int argc, char *argv[])
{
    lab2();
    return 0;
}
