#include <memory>
#include <algorithm>
#include <iostream>
#include <common.h>

class Matrix
{
private:
    int rows = 0,
        columns = 0,
        size = 0;
    std::unique_ptr<double[]> data;
public:
    Matrix();
    Matrix(int rows, int columns);
    Matrix(int rowSize, int columnSize, const double *data);

    inline int getRowsCount() const { return rows; }
    inline int getColumnsCount() const { return columns; }
    inline int getDataSize() const { return size; }

    inline double* getData() { return data.get(); }
    inline const double* getData() const { return data.get(); }

    inline double get(int i, int j) const { return data[i * columns + j]; }
    inline void set(int i, int j, double value) { data[i * columns + j] = value; }

    inline bool hasElement(int i, int j) const { return (i >= 0) && (j >= 0) && (i < rows) && (j < columns); }
    double getBorderElement(int i, int j, BorderType border) const;
    void resize(int rows, int columns);
    void normalize();
};
